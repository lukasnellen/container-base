Install build tools in containers and machines
==============================================

Scripts to set up build or run-time enviroments for CI and other applications. Uses the contents
of `/etc/os-release` to identify the linux flavour and release. The focus is on containerized
environments, but the scripts can also be used to install the tools on physical or virtual machines.

The containers are built in two steps
1) Base containers which take a bare distrubution image and provide the tools needed to build, run, and test the software. The details of this stage are distribution dependent, since they interact with the package manager and have to provide package names, which can be distribution dependend. After this stage, the container will include:
   - Runtime libraries and tools
   - Build tools if needed (currently always)
   - Additional tools for testing if needed (typically for CI)
2) Distribution independent installation of
   - Extra software needed by the project
   - The project software

**Note:** This repository contains the tools for step 1. Projects will have to provide separate code for step 2.


Physical and virtual machines
-----------------------------

You can run the scripts in `install-scripts` manualy. The
`packages.sh` script provides the list of packages that should be
installed. Physical and virtual machines should be ready to install
them directly.


Containers
----------

For containers, this uses [buildah] and [podman] as the basic tools. Containers can be
converted to [docker] or [apptainer] (before known as _singularity_).

[buildah]: https://buildah.io/
[podman]: https://buildah.io/
[docker]: https://www.docker.com/
[apptainer]: https://apptainer.org/

### Scripts

The `sysdev-prepare` script sets up the buildah containers. The
`sysdev-extend -p project` script sets up a new build container, based on an
existing container image. This can be used to build a contianer image
with additional packages, for example with analysis tools for CI.

The `run-command.sh` script runs a command inside the container. it has two shortcut options:
- `-i` runs the `install-scripts/install.sh` script with the listed project or group names. This is used to install the packages specified by the project or group names.
- `-p` runs the `install-scripts/package-names.sh` script with the listed project or group names. This is used to list the names of the packages specified by the project or group names, e.g., to generate package manitests for documentation.
- `-c` runs the `install-scripts/cleanup.sh` script, which removes temporary files from the install process to reduce the size of the container.


The `install-list` script calls `run-command.sh` first with the `-i` option to install the requested
packages and then with the `-p` option to create the package list in the `manifest` directory.
The manifest can be used, e.g., to install the same packages in physical or virtual machine.

The `sysdev-install` script queues the scripts using `nq` for parallel
execution. The `sysdev-install-one` script can be used to queue the
installation in one specific container. Either case uses the `run-list` command
to do the actual work.

The script `buildah-commit -p project` triggers some cleanup in the containers
and commits, turning them into a podman image. This image can be
converted docker format using the `podman push` command to send a copy
to the docker hub or the local daemon.

### Container naming

The `sysdev` containers contain the build tools needed. The full name is *project*/`sysdev:`*suffix*, for example `augerobservatory/sysdev:u-24.04`.

Convention for tags or suffixes: One or two letters to indicate the distribution,
followed by a dash, version number or other version indicator. Can be followed by
other indicators.

- c: CentOS
- al: Alma Linux
- r: Rocky Linux
- f: Fedora
- u: Ubuntu
- d: Debian
- s: opensuse
- a: Arch Linux (no further suffix)

Examples: `sysdev:d-10`, `sysdev:c-7`. Special cases can be:

- `sysdev:c-7scl`: centos 7 with extra compilers from softwarecollections.org
- `sysdev:d-testing`: debian testing (no version number)
- `sysdev:u-22.04clang`: ubuntu with clang instead of gcc as the compiler

### Package selection

The `install-scripts` directory contains two groups of scripts used to select the packages to install
(or report in the package manifest):

- `group-*.sh` define groups of packages, for example build support or compiler families
- `project-*.sh` is used to select the groups required by a project

Groups are, e.g., `gcc` or `clang` to select the compiler to install. **NB**: We did try `clang` only on ubuntu and debian.


Directly supported projects
---------------------------

Currently supported projects are:
- `auger` (full name `augerobservatory`): auger, auger-ci (an extended auger environment)
- `c7` (full name `corsika7`): corsika7
- `c8` (full name `corsika8`): corsika8
- `hawcobservatory`: HAWC (pending)
- `swgo`: SWGO (pending)


### Auger

#### Base containers

To get a full set of containers initialized, run
```sh
./sysdev-prepare
```
To select only on container, add a tag, for example
```sh
./sysdev-prepare u 22.04
```
to set up a container for Ubuntu 22.04.

Then install the packages, commit, and remove:
```sh
./install-list u-22.04 auger
./buildah-commit -p auger u-22.04
./remove-all u-22.04
```

If you want to process a large number of containers, use
```sh
./sysdev-install auger # Check results in the queue directory
./buildah-commit -p auger
./remove-all
```
For queuing, we use the [nq](https://github.com/leahneukirchen/nq), which is available at least on debian and ubuntu. The job file and output is kept in the `queue` directory.

#### Extended container for auger CI

The CI for Auger requires some additional packages. To extend the base container, run:
```sh
./sysdev-extend -p auger u-22.04 u-22.04-ci
./install-list u-22.04-ci auger-ci
./buildah-commit -p auger u-22.04-ci
./remove-all u-22.04-ci
```

### CORSIKA 8

We have support to build containers with a common base. Each step triggers the
installation of packages in the container(s) and has to be followed by the
commonm final steps.

#### Base containers

Prepare the base container and trigger package installation:

```sh
./project-corsika8.sh -b
```

#### Containers to build with gcc

First make sure the base containers were built successfully. Then run

```sh
./project-corsika8.sh -g
```

#### Containers to build with clang

First make sure the base containers were built successfully. Then run

```sh
./project-corsika8.sh -c
```

#### Common final steps

Look at the files in `queue` directory to see if the installation(s) was
successful. To commit and clean up, execute

```sh
./buildah-commit -p c8
./remove-all
```
