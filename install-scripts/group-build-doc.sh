#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	doxygen
	graphviz
	sphinx-doc
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	cat <<-EOF
	doxygen
	python3-sphinx
	EOF
	;;

    arch)
	cat <<-EOF
	doxygen
	graphviz
	python-sphinx
	EOF
	;;
esac
