#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID-$VERSION_ID" in
    centos-7)
	DNF=yum
	;;
    *)
	DNF=dnf
	;;
esac

case "$ID" in
    debian|ubuntu)
	apt-get clean
	rm -rf /var/lib/apt/lists/*
	;;

    rhel|fedora|centos|almalinux|rocky)
	$DNF clean all
	rm -rf /var/cache/yum/*
	rm -rf /var/cache/dnf/*
	;;

    arch)
	pacman -Scc --noconfirm
	rm -rf /var/cache/pacman/pkg/*
	;;
esac
