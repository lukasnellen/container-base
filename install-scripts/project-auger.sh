#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

groups="tools build gcc boost-build root-dev mysql"
for g in ${groups}; do
    $DIR/group-${g}.sh
done
