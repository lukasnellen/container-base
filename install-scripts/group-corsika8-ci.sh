#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	python3-pip
	cmake
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	cat <<-EOF
	python3-pip
	cmake
	EOF
	;;

    arch)
	cat <<-EOF
	python-pip
	cmake
	EOF
	;;
esac
