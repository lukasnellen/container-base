#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

groups="build gcc"
for g in ${groups}; do
    $DIR/group-${g}.sh
done
