#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	clang
	gfortran
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
		case "$VERSION_ID" in
	    7)
		echo >&2 Flavour $ID not set up for clang
		exit 1
		;;
	    *)
		cat <<-EOF
		clang
		gcc-gfortran
		EOF
		;;
	esac
	;;

    arch)
	cat <<-EOF
	clang
	gcc-fortran
	EOF
	;;
esac
