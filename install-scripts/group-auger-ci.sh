#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	python3-matplotlib
	python3-numpy
	gnuplot-nox
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	echo >&2 Flavour $ID not set up for auger CI
	exit 1
	;;

    arch)
	echo >&2 Flavour $ID not set up for auger CI
	exit 1
	;;
esac
