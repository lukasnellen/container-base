#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	binutils
	debianutils
	dpkg-dev
	doxygen
	git
	make
	ninja-build
	patch
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	cat <<-EOF
	binutils
	doxygen
	git
	make
	ninja-build
	patch
	gawk
	EOF
	;;

    arch)
	cat <<-EOF
	binutils
	doxygen
	git
	make
	ninja
	patch
	EOF
	;;
esac
