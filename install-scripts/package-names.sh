#! /bin/sh
#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

if [ -z "$1" ]; then
    $DIR/project-common.sh
else
    for c in "$@"; do
	if [ -e "$DIR/project-${c}.sh" ]; then
	    $DIR/project-${c}.sh
	else
	    $DIR/group-${c}.sh
	fi
    done
fi
