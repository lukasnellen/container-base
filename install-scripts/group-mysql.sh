#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	case "$ID-$VERSION_ID" in
	    ubuntu-*) echo libmysqlclient-dev ;;
	    debian-*) echo default-libmysqlclient-dev ;;
	esac
	;;

    rhel|fedora|centos|almalinux|rocky)
	case "$ID" in
	    fedora)
		echo community-mysql-devel
		;;
	    *)
		echo mariadb-devel
		;;
	esac
	;;

    arch)
	echo libmariadbclient
	;;
esac
