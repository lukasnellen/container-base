#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID-$VERSION_ID" in
    ubuntu-18.04)
	echo python
	;;
    debian-*|ubuntu-*)
	echo libopenmpi-dev
	;;

    rhel-*|centos-*|almalinux-*|rocky-*|fedora-*)
	echo openmpi-devel
	;;


    arch-*)
	echo openmpi
	;;

esac
