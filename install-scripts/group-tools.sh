#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release


case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	less
	rsync
	file
	wget
	openssl
	libssl-dev
	rsync
	ssh-client
	python3
	python3-virtualenv
	python3-venv
	python3-dev
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	cat <<-EOF
	findutils
	less
	rsync
	which
	file
	wget
	openssl
	openssl-devel
	rsync
	EOF
	case "$ID-$VERSION_ID" in
	    fedora-*)
		cat <<-EOF
		python3
		python3-devel
		python3-virtualenv
		EOF
		;;
	    *-7|*-7.*)
		cat <<-EOF
		python
		python-devel
		python-virtualenv
		rh-python38
		EOF
		;;
	    *-8|*-8.*)
		cat <<-EOF
		python3
		python3-devel
		python3-virtualenv
		EOF
		;;
	    *-9|*-9.*)
		cat <<-EOF
		python3
		python3-devel
		EOF
		;;
	esac
	;;

    arch)
	cat <<-EOF
	less
	rsync
	file
	which
	wget
	openssl
	rsync
	python
	EOF
	;;
esac
