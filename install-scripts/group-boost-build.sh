#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	libbz2-dev
	zlib1g-dev
	libssl-dev
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	cat <<-EOF
	bzip2-devel
	zlib-devel
	openssl-devel
	EOF
	;;

    arch)
	cat <<-EOF
	bzip2
	zlib
	openssl
	EOF
	;;
esac
