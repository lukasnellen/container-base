#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	gcc
	g++
	gfortran
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	case "$VERSION_ID" in
	    7)
		cat <<-EOF
		devtoolset-11
		EOF
		;;
	    *)
		cat <<-EOF
		gcc
		gcc-c++
		gcc-gfortran
		EOF
		;;
	esac
	;;

    arch)
	cat <<-EOF
	gcc
	gcc-fortran
	EOF
	;;
esac
