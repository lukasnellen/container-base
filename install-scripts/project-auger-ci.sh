#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

$DIR/project-auger.sh
$DIR/group-auger-ci.sh
