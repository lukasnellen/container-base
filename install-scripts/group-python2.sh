#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID-$VERSION_ID" in
    ubuntu-18.04)
	echo python
	;;
    debian-*|ubuntu-*)
	echo python2
	;;

    rhel-9.*|centos-9|almalinux-9.*|rocky-9.*)
	echo >&2 Flavour $ID without support for python 2
	exit 1
	;;

    fedora-*)
	echo python2.7
	;;


    rhel-*|centos-*|almalinux-*|rocky-*)
	echo python2
	;;


    arch-*)
	echo python2
	;;

esac
