#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

groups="tools build build-doc build-openssl"
for g in ${groups}; do
    $DIR/group-${g}.sh
done
