#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    rhel|fedora|centos|almalinux|rocky)
	case "$ID-$VERSION_ID" in
		*-8|*-8.*)
			cat <<-EOF
			perl
			perl-IPC-Cmd
			perl-Digest-SHA
			texinfo
			EOF
			;;
		*-9|*-9.*|*-10|*-10.*|fedora-*)
			cat <<-EOF
			perl
			perl-FindBin
			perl-IPC-Cmd
			perl-File-Compare
			perl-File-Copy
			perl-Digest-SHA
			EOF
			;;
		esac
		;;
	arch)
		cat <<-EOF
		texinfo
		EOF
		;;
esac
