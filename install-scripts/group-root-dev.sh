#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID" in
    debian|ubuntu)
	cat <<-EOF
	libpng-dev
	libjpeg-dev
	libx11-dev
	libxpm-dev
	libxft-dev
	libxext-dev
	EOF
	;;

    rhel|fedora|centos|almalinux|rocky)
	cat <<-EOF
	libX11-devel
	libXpm-devel
	libXft-devel
	libXext-devel
	EOF
	;;

    arch)
	cat <<-EOF
	libpng
	libx11
	libxpm
	libxft
	libxext
	EOF
	;;
esac
