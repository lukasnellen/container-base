#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID-$VERSION_ID" in
    centos-7)
	DNF=yum
	;;
    *)
	DNF=dnf
	;;
esac

debian_prepare () {
    # Recent ubuntu container cleanup fails
    if [ -e "/etc/apt/apt.conf.d/docker-clean" ]; then
	rm /etc/apt/apt.conf.d/docker-clean
    fi

    ln -snf /usr/share/zoneinfo/America/Mexico_City /etc/localtime
    echo /etc/timezoneecho "America/Mexico_city" > /etc/timezone
}

rhel_prepare () {
    case "$VERSION_ID" in
	7)
            $DNF install -y epel-release centos-release-scl
            ;;
	8|8.*)
            $DNF install -y dnf-utils
            $DNF config-manager --set-enabled powertools
            ;;
	9|9.*|10|10.*)
            $DNF install -y dnf-utils
            $DNF config-manager --set-enabled crb
            ;;
    esac
}

case "$ID" in
    debian|ubuntu)
	debian_prepare
	apt-get update
	apt-get install --no-install-recommends --yes $($DIR/package-names.sh "$@")
	;;

    rhel|fedora|centos|almalinux|rocky)
	rhel_prepare
	$DNF install -y $($DIR/package-names.sh "$@")
	;;

    arch)
	pacman --noconfirm --noprogressbar -Syu $($DIR/package-names.sh "$@")
	;;
esac
