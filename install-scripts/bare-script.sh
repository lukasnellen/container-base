#! /bin/sh

set -e
#set -x

DIR=$(realpath $(dirname $0))

. /etc/os-release

case "$ID-$VERSION_ID" in
    centos-7)
	DNF=yum
	;;
    *)
	DNF=dnf
	;;
esac


case "$ID" in
    debian|ubuntu)
	;;

    rhel|fedora|centos|almalinux|rocky)
	;;

    arch)
	;;
esac
