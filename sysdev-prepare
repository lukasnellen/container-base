#! /bin/bash

#set -x
set -e

CENTOS=(stream9 stream10)
#CENTOS_SCL=(7)

FEDORA=(39 40 41 rawhide) # rawhide currently is 42

ALMA=(8 9 10-kitten)
ROCKY=(8 9)

DEBIAN=(10 11 12 testing) # testing is currently 13

UBUNTU=(20.04 22.04 24.04 24.10 devel) # devel currently is 25.04

containers=$(mktemp) || exit
trap "rm -f -- '${containers}'" EXIT
buildah ls >>${containers}

exists()
{
    egrep -q $1\$ ${containers}
}

rh()
{
    name=sysdev:$2-$3$4
    exists ${name} && return
    buildah from --security-opt=seccomp=unconfined --name ${name} $1:$3
}

debian()
{
    name=sysdev:d-$1$2
    exists ${name} && return
    buildah from --security-opt=seccomp=unconfined --name ${name} debian:$1
}

gcc()
{
    name=sysdev:gcc-$1
    exists ${name} && return
    buildah from --security-opt=seccomp=unconfined --name ${name} gcc:$1
}

ubuntu()
{
    name=sysdev:u-$1$2
    exists ${name} && return
    buildah from --security-opt=seccomp=unconfined --name ${name} ubuntu:$1
}

arch()
{
    name=sysdev:a$1
    exists ${name} && return
    buildah from --security-opt=seccomp=unconfined --name ${name} archlinux
}

prepare_all()
{
    for i in ${CENTOS[@]}; do rh centos c $i; done
    #for i in ${CENTOS_SCL[@]}; do rh centos c $i scl; done
    for i in ${FEDORA[@]}; do rh fedora f $i; done
    for i in ${ALMA[@]}; do rh almalinux al $i; done
    for i in ${ROCKY[@]}; do rh rockylinux r $i; done
    for i in ${DEBIAN[@]}; do debian $i; done
    for i in ${UBUNTU[@]}; do ubuntu $i; done
    arch
}

prepare_one()
{
    code=$1
    shift;
    case ${code} in
	c) rh centos c $@ ;;
	f) rh fedora f $@ ;;
	r) rh rockylinux r $@ ;;
	al) rh almalinux al $@ ;;
	d) debian $@ ;;
	u) ubuntu $@ ;;
	a) arch $@ ;;
	gcc) gcc $@ ;;
    esac
}

if [ -n "$1" ]; then
    prepare_one $@
else
    prepare_all
fi

rm -f -- "${containers}"
trap - EXIT
