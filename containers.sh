#! /bin/bash

set -e

DIR=$(realpath $(dirname $0))

buildah ls | awk '$5 ~ /sysdev/ { print $5 }' | sort
buildah ls | awk '$4 ~ /^sysdev/ { print $4 }' | sort
