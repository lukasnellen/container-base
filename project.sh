if [ -n "$1" ]; then
    project=$1
else
    project=${PROJECT}
fi

case ${project} in
c7)
    project=corsika7
    ;;
c8)
    project=corsika8
    ;;
auger)
    project=augerobservatory
    ;;
esac
