#! /bin/sh

DIR=$(realpath $(dirname $0))

usage () {
    echo >&2 "Usage: $0 [-i] [-p] tag commmand..."
}

command_prefix=
command=bash

while getopts ipc opt; do
    case "$opt" in
	i) command_prefix=/install-scripts/
       command=install.sh
       ;;
	p) command_prefix=/install-scripts/
       command=package-names.sh
       ;;
	c) command_prefix=/install-scripts/
       command=cleanup.sh
       ;;
	*) usage
	   exit 1
	   ;;
    esac
done
shift $((OPTIND - 1))

if [ -z "$1" ]; then
    usage
    exit
fi


tag=sysdev:${1#sysdev:}
shift


buildah run -v $DIR/install-scripts:/install-scripts \
	--hostname $tag \
	--network=host \
	$tag "${command_prefix}${command}" "$@"
