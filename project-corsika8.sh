#! /bin/bash
# Prepare installation of containers for CORSIKA 8

#set -x
set -e

DIR=$(realpath $(dirname $0))

export PROJECT=corsika8

BASES=('a' 'al-9' 'u-22.04' 'u-24.04' 'd-12' 'f-41' 'f-42')

case "$1" in
# Base containers
-b) for b in ${BASES[@]}; do
        ${DIR}/sysdev-prepare ${b/-/ } -base
    done
    ${DIR}/sysdev-install corsika8-ci corsika8-base
    ;;
# gcc
-g) for b in ${BASES[@]}; do
        ${DIR}/sysdev-extend ${b}-base ${b}
    done
    ${DIR}/sysdev-install corsika8
    ;;
# clang
-c) for b in ${BASES[@]}; do
        ${DIR}/sysdev-extend ${b}-base ${b}-clang
    done
    ${DIR}/sysdev-install corsika8-clang
    ;;
*)  echo "Unknown argument '$1'"
    exit 1
    ;;
esac
